CREATE TABLE "user"
(
    "id" varchar(36) NOT NULL,
    "created" timestamp NOT NULL,

    "username" varchar(255) NOT NULL UNIQUE,

    PRIMARY KEY ("id")
);

CREATE TABLE "task"
(
    "id" varchar(36) NOT NULL,
    "created" timestamp NOT NULL,

    "task" varchar(1023) NOT NULL,
    "user_id" varchar(36) REFERENCES "user" ("id"),
    "completed" bool NOT NULL,

    PRIMARY KEY ("id")
);
