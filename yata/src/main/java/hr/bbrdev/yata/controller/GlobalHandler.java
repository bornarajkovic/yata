package hr.bbrdev.yata.controller;

import hr.bbrdev.yata.controller.exception.MessageException;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.LocalDateTime;

@ControllerAdvice
public class GlobalHandler {

    @AllArgsConstructor
    public static class MessageResponse {
        @Getter private final String message;
        @Getter private final LocalDateTime created;

        private static MessageResponse fromException(MessageException ex) {
            return new MessageResponse(
                    ex.getMessage(),
                    ex.getCreated()
            );
        }
    }

    @ExceptionHandler({MessageException.class})
    public final ResponseEntity<MessageResponse> handleBaseMessageException(final MessageException ex) {
        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<>(MessageResponse.fromException(ex), headers, ex.getStatus());
    }

}
