package hr.bbrdev.yata.controller.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

public class MessageException extends RuntimeException {
    @Getter private final String message;
    @Getter private final LocalDateTime created;
    @Getter private final HttpStatus status;

    public MessageException(String message, HttpStatus status) {
        this.created = LocalDateTime.now();
        this.message = message;
        this.status = status;
    }
}
