package hr.bbrdev.yata.controller;

import hr.bbrdev.yata.model.User;
import hr.bbrdev.yata.service.UserService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/users")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/{username}")
    public User create(@PathVariable String username) {
        return userService.create(username);
    }

    @GetMapping("/{username}")
    public User get(@PathVariable String username) {
        return userService.get(username);
    }
}
