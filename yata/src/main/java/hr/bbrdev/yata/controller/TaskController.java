package hr.bbrdev.yata.controller;

import hr.bbrdev.yata.model.Task;
import hr.bbrdev.yata.model.User;
import hr.bbrdev.yata.service.TaskService;
import hr.bbrdev.yata.service.UserService;
import lombok.Data;
import org.springframework.web.bind.annotation.*;

@RestController
public class TaskController {

    private final TaskService taskService;
    private final UserService userService;

    public TaskController(TaskService taskService,
                          UserService userService) {
        this.taskService = taskService;
        this.userService = userService;
    }

    @Data
    private static class TaskRequest {
        String task;
    }

    @PostMapping("/api/users/{username}/tasks")
    public Task create(@RequestBody TaskRequest task, @PathVariable String username) {
        User user = userService.get(username);
        return taskService.create(user, task.getTask());
    }

    @PutMapping("/api/users/{username}/tasks/{taskId}/complete")
    public Task complete(@PathVariable String taskId, @PathVariable String username) {
        User user = userService.get(username);
        return taskService.complete(taskId);
    }

    @GetMapping("/api/users/{username}/tasks/{taskId}")
    public Task get(@PathVariable String taskId, @PathVariable String username) {
        User user = userService.get(username);
        return taskService.get(taskId);
    }

    @DeleteMapping("/api/users/{username}/tasks/{taskId}")
    public Task delete(@PathVariable String taskId, @PathVariable String username) {
        User user = userService.get(username);
        return taskService.delete(taskId);
    }

}
