package hr.bbrdev.yata.dao;

import hr.bbrdev.yata.model.Task;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TaskDao extends JpaRepository<Task, String> { }
