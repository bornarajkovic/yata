package hr.bbrdev.yata;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YetAnotherTodoApplication {

	public static void main(String[] args) {
		SpringApplication.run(YetAnotherTodoApplication.class, args);
	}

}
