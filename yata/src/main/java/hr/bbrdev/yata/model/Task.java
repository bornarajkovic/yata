package hr.bbrdev.yata.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Data
@Entity
@NoArgsConstructor
public class Task extends BaseEntity {

    String task;

    boolean completed;

    @JsonIgnore
    @ManyToOne
    User user;

    public Task(String task, User user) {
        this.task = task;
        this.user = user;
        this.completed = false;
    }
}
