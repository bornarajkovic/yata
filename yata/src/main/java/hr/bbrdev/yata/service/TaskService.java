package hr.bbrdev.yata.service;

import hr.bbrdev.yata.model.Task;
import hr.bbrdev.yata.model.User;

public interface TaskService {
    Task create(User user, String task);

    Task get(String taskId);

    Task complete(String taskId);

    Task delete(String taskId);
}
