package hr.bbrdev.yata.service.impl;

import hr.bbrdev.yata.controller.exception.MessageException;
import hr.bbrdev.yata.dao.UserDao;
import hr.bbrdev.yata.model.User;
import hr.bbrdev.yata.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserServiceImpl implements UserService {

    private final UserDao userDao;

    public UserServiceImpl(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public User create(String username) {
        if(userDao.existsByUsername(username)) {
            throw new MessageException("User already exists", HttpStatus.BAD_REQUEST);
        }
        return userDao.save(new User(username));
    }

    @Override
    @Transactional
    public User get(String username) {
        return userDao.findByUsername(username).orElseThrow(() -> new MessageException("User doesn't exist", HttpStatus.NOT_FOUND));
    }
}
