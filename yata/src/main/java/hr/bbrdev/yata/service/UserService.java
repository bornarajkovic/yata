package hr.bbrdev.yata.service;

import hr.bbrdev.yata.model.User;

public interface UserService {
    User create(String username);

    User get(String username);
}
