package hr.bbrdev.yata.service.impl;

import hr.bbrdev.yata.controller.exception.MessageException;
import hr.bbrdev.yata.dao.TaskDao;
import hr.bbrdev.yata.model.Task;
import hr.bbrdev.yata.model.User;
import hr.bbrdev.yata.service.TaskService;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class TaskServiceImpl implements TaskService {

    private final TaskDao taskDao;

    public TaskServiceImpl(TaskDao taskDao) {
        this.taskDao = taskDao;
    }

    @Override
    public Task create(User user, String task) {
        return taskDao.save(new Task(task, user));
    }

    @Override
    public Task get(String taskId) {
        return taskDao.findById(taskId).orElseThrow(() -> new MessageException("No task with given id", HttpStatus.NOT_FOUND));
    }

    @Override
    public Task complete(String taskId) {
        Task task = this.get(taskId);
        task.setCompleted(true);
        return taskDao.save(task);
    }

    @Override
    public Task delete(String taskId) {
        Task task = this.get(taskId);
        taskDao.delete(task);
        return task;
    }
}
