import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {UserService} from './user.service';
import {Observable} from 'rxjs';
import {Task} from '../model/task';
import {environment} from '../../environments/environment';
import {switchMap, take} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  constructor(private httpClient: HttpClient,
              private userService: UserService) {
  }

  create(task: string): Observable<Task> {
    return this.userService.user$.pipe(take(1), switchMap(user => {
      return this.httpClient.post<Task>(environment.api + '/users/' + user?.username + '/tasks', {task});
    }));
  }

  complete(taskId: string): Observable<Task> {
    return this.userService.user$.pipe(take(1), switchMap(user => {
      return this.httpClient.put<Task>(environment.api + '/users/' + user?.username + '/tasks/' + taskId + '/complete', null);
    }));
  }

  get(taskId: string): Observable<Task> {
    return this.userService.user$.pipe(take(1), switchMap(user => {
      return this.httpClient.get<Task>(environment.api + '/users/' + user?.username + '/tasks/' + taskId);
    }));
  }

  delete(taskId: string): Observable<Task> {
    return this.userService.user$.pipe(take(1), switchMap(user => {
      return this.httpClient.delete<Task>(environment.api + '/users/' + user?.username + '/tasks/' + taskId);
    }));
  }
}
