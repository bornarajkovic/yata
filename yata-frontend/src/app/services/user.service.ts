import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, ReplaySubject } from 'rxjs';
import { User } from '../model/user';
import { environment } from '../../environments/environment';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private user: ReplaySubject<User> = new ReplaySubject<User>(1);

  constructor(private httpClient: HttpClient) {
    const username = sessionStorage.getItem('username');
    if (username) {
      this.get(username).subscribe(user => this.setUser(user), () => {
        sessionStorage.removeItem('username');
        this.setUser(null);
      });
    } else {
      this.setUser(null);
    }

    this.user.subscribe(user => {
      if (user == null) {
        sessionStorage.removeItem('username');
      } else {
        sessionStorage.setItem('username', user.username);
      }
    });
  }

  get user$(): Observable<User> {
    return this.user.asObservable();
  }

  public login(username: string): Observable<User> {
    return this.get(username).pipe(tap(user => this.setUser(user)));
  }

  public logout(): void {
    this.setUser(null);
  }

  public refresh(): void {
    const username = sessionStorage.getItem('username') as string;
    this.get(username).subscribe(user => this.setUser(user));
  }

  public register(username: string): Observable<User> {
    return this.httpClient.post<User>(environment.api + '/users/' + username, null).pipe(tap(user => this.setUser(user)));
  }

  private get(username: string): Observable<User> {
    return this.httpClient.get<User>(environment.api + '/users/' + username);
  }

  private setUser(user: User | null): void {
    if (user && user.tasks === null) {
      user.tasks = [];
    }
    user?.tasks.reverse();
    console.log(user);
    this.user.next(user as any as User);
  }
}
