import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EnvironmentService {

  constructor() { }

  init(): Promise<any> {
    return fetch('/assets/env.json').then(r => r.json()).then(env => {
      environment.api = env.api || environment.api;
    }).catch(() => {
      console.error('Using default environment');
    });
  }
}
