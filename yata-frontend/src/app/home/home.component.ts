import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {Observable} from 'rxjs';
import {UserService} from '../services/user.service';
import {TaskService} from '../services/task.service';
import {User} from '../model/user';
import {Task} from '../model/task';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  user$: Observable<User>;
  showUnfinished = true;
  showFinished = true;
  taskText = '';

  @ViewChild('taskDialog', {static: false}) taskDialog: TemplateRef<any> = null as any as TemplateRef<any>;
  private taskDialogInstance: MatDialogRef<any> = null as any as MatDialogRef<any>;

  constructor(private userService: UserService,
              private taskService: TaskService,
              private dialog: MatDialog) {
    this.user$ = userService.user$;
  }

  ngOnInit(): void {
  }


  filtered(tasks: Task[], showUnfinished: boolean, showFinished: boolean): Task[] {
    return tasks.filter(t => (t.completed && showFinished) || (!t.completed && showUnfinished));
  }

  createTask(taskText: string): void {
    this.taskService.create(taskText).subscribe(response => {
      this.taskDialogInstance.close();
      this.taskText = '';
    });
  }

  openTaskDialog(): void {
    this.taskDialogInstance = this.dialog.open(this.taskDialog);
    this.taskDialogInstance.afterClosed().subscribe(() => this.userService.refresh());
  }

  completeTask(task: Task): void {
    this.taskService.complete(task.id).subscribe(response => {
      task.completed = response.completed;
    });
  }

  deleteTask(task: Task): void {
    this.taskService.delete(task.id).subscribe(() => {
      this.userService.refresh();
    });
  }

  logout(): void {
    this.userService.logout();
  }
}
