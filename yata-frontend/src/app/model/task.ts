export class Task {
  constructor(public id: string,
              public created: string,
              public task: string,
              public completed: boolean) {}
}
