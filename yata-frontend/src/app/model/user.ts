import { Task } from './task';

export class User {
  constructor(public id: string,
              public created: string,
              public username: string,
              public tasks: Task[]) {}
}
