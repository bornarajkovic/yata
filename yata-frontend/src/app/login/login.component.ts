import { Component, OnInit } from '@angular/core';
import {UserService} from '../services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginUsername = '';
  registerUsername = '';

  loginError: string | null = null;
  registerError: string | null = null;

  constructor(private userService: UserService) { }

  ngOnInit(): void {
  }

  register(): void {
    this.registerError = null;
    this.userService.register(this.registerUsername)
        .subscribe(() => {},
        error => this.registerError = error.error.message);
  }

  login(): void {
    this.loginError = null;
    this.userService.login(this.loginUsername)
      .subscribe(() => {},
        error => this.loginError = error.error.message);
  }
}
